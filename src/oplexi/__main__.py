from oplexi.grammar import Grammar, Quantifier
from oplexi.speaker import Speaker, generate_lexicon, generate_world
from oplexi.learner import transfer
from oplexi.parameters import GENERATIONS, SENTENCES_PER_GENERATION, LEXICALIZATION_WINDOW_SIZE, LEXICALIZATION_THRESHOLD, \
    BASIC_OPERATORS


def main():
    base_lexicon = generate_lexicon()
    world = generate_world(base_lexicon)
    grammar = Grammar(base_lexicon)

    simplexes = {word: Quantifier(word, getattr(Grammar, operator)) for operator, word in BASIC_OPERATORS.items()}
    grammar.lexicon.update(simplexes)

    speaker = Speaker(grammar,
                      sentences_per_speech=SENTENCES_PER_GENERATION,
                      lexicalization_window_size=LEXICALIZATION_WINDOW_SIZE,
                      lexicalization_rate=LEXICALIZATION_THRESHOLD)

    simplex_operators = {getattr(Grammar, operator) for operator in BASIC_OPERATORS}
    for i in range(GENERATIONS):
        print(sorted(map(lambda quantifier: quantifier.operator.name, grammar.get_lexemes(Quantifier).values())))
        grammar = transfer(world, base_lexicon, speaker, simplex_operators)
        speaker = Speaker(grammar,
                          sentences_per_speech=SENTENCES_PER_GENERATION,
                          lexicalization_window_size=LEXICALIZATION_WINDOW_SIZE,
                          lexicalization_rate=LEXICALIZATION_THRESHOLD)

main()
