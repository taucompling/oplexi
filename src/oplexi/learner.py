import random
import itertools

from oplexi.grammar import Grammar, Quantifier, Node


def translate(grammar, sentence):
    translated_sentence = []
    for element in sentence:
        if isinstance(element, Node):
            translated_sentence.append(grammar.lexicon[element.name])
        else:
            translated_sentence.append(element)
    return translated_sentence


def is_valid_grammar(grammar, sentences, world):
    for sentence in sentences:
        translated_sentence = translate(grammar, sentence)
        if not grammar.evaluate(world, translated_sentence):
            return False
    return True


def transfer(world, lexicon, speaker, simplexes):
    sentences = speaker.speak(world)

    elements = itertools.chain.from_iterable(sentences)
    nodes = map(lambda node: node.name, filter(lambda element: isinstance(element, Node), elements))
    unlexicalized_words = list(set(filter(lambda x: x not in lexicon, nodes)))
    operators = simplexes | set(map(lambda x: x.not_, simplexes))

    operator_allocations = set(itertools.chain.from_iterable(
        map(itertools.permutations, itertools.combinations_with_replacement(operators, len(unlexicalized_words)))))

    operator_allocations = list(operator_allocations)
    random.shuffle(operator_allocations)

    for allocation in operator_allocations:
        child_grammar = Grammar(lexicon)

        quantifiers = {}
        for operator, word in zip(allocation, unlexicalized_words):
            quantifiers[word] = Quantifier(word, operator)
        child_grammar.lexicon.update(quantifiers)

        if is_valid_grammar(child_grammar, sentences, world):
            break
    else:
        raise Exception("Could not create grammar")

    return child_grammar

