import math

import itertools


class Node(object):
    def __init__(self, name, argtypes, func):
        self.name = name
        self.argtypes = argtypes
        self.func = func

    def __repr__(self):
        return "<{} '{}'>".format(self.__class__.__name__, self.name)

    @property
    def complexity(self):
        return 0


class Individual(Node):
    def __init__(self, name):
        def func(w, argument):
            return self

        Node.__init__(self, name, (), func)


class Predicate(Node):
    def __init__(self, name):
        def func(w, argument):
            return argument[0] in w[name]

        Node.__init__(self, name, (Individual,), func)


class Quantifier(Node):
    def __init__(self, name, operator):
        def func(w, argument):
            individuals = filter(lambda x: isinstance(x, Individual), w.values())
            qrange = filter(lambda x: argument[0].func(w, (x,)), individuals)
            truth_values = [argument[1].func(w, (x,)) for x in qrange]
            return operator(truth_values)

        self.mark = 0
        self.operator = operator
        Node.__init__(self, name, (Predicate, Predicate), func)

    def __eq__(self, other):
        return isinstance(other, Quantifier) and self.name == other.name and self.operator == other.operator

    def __hash__(self):
        return hash(self.operator) + hash(self.name)

    def __repr__(self):
        return "<{} {}({})>".format(self.__class__.__name__, self.name, self.operator.name)


class Operator(object):
    def __init__(self, name, func):
        self._name = name
        self._func = func
        self._negated_operator = None

    @property
    def name(self):
        return self._name

    @property
    def not_(self):
        if self._negated_operator is None:
            self._negated_operator = Operator('not-{}'.format(self.name), lambda s: not self._func(s))
            self._negated_operator._negated_operator = self
        return self._negated_operator

    def __call__(self, s):
        return self._func(s)

    def __eq__(self, other):
        return isinstance(other, Operator) and self._func == other._func

    def __hash__(self):
        return hash(self._func)

    def __repr__(self):
        return "<Operator '{}'>".format(self.name)


def not_(node):
    if isinstance(node, Predicate):
        res = Predicate('not-{}'.format(node.name))
        res.func = lambda w, argument: not node.func(w, argument)
    elif isinstance(node, Quantifier):
        res = Quantifier('not-{}'.format(node.name), node.operator.not_)
    else:
        raise TypeError

    return res


class Grammar(object):
    A = Operator('A', lambda s: False not in s)
    I = Operator('I', lambda s: True in s)
    E = I.not_
    E._name = 'E'
    O = A.not_
    O._name = 'O'
    Y = Operator('Y', lambda s: True in s and False in s)
    SQUARE = (A, E, O, I)
    INTENTIONS = {operator.name: operator for operator in (A, Y, E, O, I)}
    CONTRARIES = {
        A: E,
        E: A,
        I: O,
        O: I
    }

    def __init__(self, lexicon):
        self.lexicon = lexicon.copy()

    def how_to_say(self, operator, qrange, expression):
        if operator == Grammar.Y:
            how_to_say_I = self.how_to_say(Grammar.I, qrange, expression)
            how_to_say_O = self.how_to_say(Grammar.O, qrange, expression)
            how_to_say_IO = list(map(lambda x: x[0] + x[1], itertools.product(how_to_say_I, how_to_say_O)))
            return how_to_say_I + how_to_say_O + how_to_say_IO
        else:
            quantifiers = self.get_lexemes(Quantifier).values()
            cont = Grammar.CONTRARIES[operator]
            l = []
            for quantifier in quantifiers:
                if quantifier.operator == operator:
                    l.append((quantifier, qrange, expression))
                elif quantifier.operator == cont:
                    l.append((quantifier, qrange, not_, expression))
                elif quantifier.operator.not_ == operator:
                    l.append((not_, quantifier, qrange, expression))
                elif quantifier.operator.not_ == cont:
                    l.append((not_, quantifier, qrange, not_, expression))

        return l

    @staticmethod
    def evaluate(w, expression):
        l = []
        for i, elem in enumerate(expression):
            if i > 0 and expression[i - 1] is not_:
                l.append(not_(elem))
            elif elem is not not_:
                l.append(elem)

        truth = True
        for i in range(0, len(l), 3):
            truth = truth and l[i].func(w, l[i + 1: i + 3])
        return truth

    def get_lexemes(self, type_=Node):
        """

        :param type_: class or type
        :return: dict
        """
        return dict(((k, v) for (k, v) in self.lexicon.items() if isinstance(v, type_)))
