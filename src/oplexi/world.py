import random
from oplexi.grammar import Individual, Predicate


def generate_word():
    vowels = ['a', 'e', 'u', 'o', 'i']
    consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y',
                  'z']

    s = []
    for i in range(random.randint(2, 7)):
        if i % 2 == 0:
            s.append(random.choice(consonants))
        else:
            s.append(random.choice(vowels))

    return str().join(s)


def generate_predicate():
    return Predicate(generate_word())


def generate_individual():
    return Individual(generate_word().capitalize())


def generate_world(lexicon):
    individuals = list(filter(lambda x: isinstance(x, Individual), lexicon.values()))
    predicates = list(filter(lambda x: isinstance(x, Predicate), lexicon.values()))

    world = {}
    for predicate in predicates:
        random.shuffle(individuals)
        world[predicate.name] = individuals[:random.randint(1, len(individuals))]

    for individual in individuals:
        world[individual.name] = individual

    world[predicates[0]] = list(world[predicates[1].name])
    world[predicates[2]] = []

    return world


def generate_lexicon():
    lexicon = dict()

    for _ in range(10):
        individual = generate_individual()
        lexicon[individual.name] = individual

    for _ in range(10):
        predicate = generate_predicate()
        lexicon[predicate.name] = predicate

    return lexicon
