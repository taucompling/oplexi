import random
import math

from oplexi.parameters import INTENTION_WEIGHTS, COMPLEXITY_COEFFICIENT
from oplexi.grammar import Predicate, Quantifier, Grammar, not_
from oplexi.world import generate_world, generate_lexicon


def _complexity_weight(sentence):
    connector_count = sum(isinstance(e, Quantifier) for e in sentence) - 1
    operator_count = sum((isinstance(e, Quantifier) or e is not_) for e in sentence) + connector_count
    return 1. / COMPLEXITY_COEFFICIENT ** operator_count


def _weighted_choice(choices):
    total = sum(choices.values())
    r = random.uniform(0, total)
    base = 0
    for c, w in choices.items():
        if r <= base + w:
            return c
        base += w
    assert False, "Shouldn't get here"


class Speaker(object):
    def __init__(self, grammar, sentences_per_speech, lexicalization_window_size, lexicalization_rate):
        self.grammar = grammar
        self._lexicalization_window_size = lexicalization_window_size
        self._lexicalization_rate = lexicalization_rate
        self._sentences_per_speech = sentences_per_speech

    def lexicalize(self, recent_quantifiers):
        quantifier_counts = {}
        for quantifier in recent_quantifiers:
            if quantifier not in quantifier_counts:
                quantifier_counts[quantifier] = 0
            quantifier_counts[quantifier] += 1

        lexicalized_quantifiers = self.grammar.get_lexemes(Quantifier).values()
        lexicalized_operators = set((quantifier.operator for quantifier in lexicalized_quantifiers))
        for quantifier, count in quantifier_counts.items():
            if count > self._lexicalization_rate:
                if quantifier.operator not in lexicalized_operators:
                    self.grammar.lexicon[quantifier.name] = quantifier

    def speak(self, world):
        sentences = []
        recent_quantifiers = []
        stats = {}
        for i in range(self._sentences_per_speech):
            intention, sentence = self.generate_true_sentence(world)
            sentences.append(sentence)

            used_quantifiers = []
            for index, element in enumerate(sentence):
                if isinstance(element, Quantifier):
                    quantifier = element
                    used_quantifiers.append(quantifier)

                if element is not_ and isinstance(sentence[index + 1], Quantifier):
                    quantifier = not_(sentence[index + 1])
                    used_quantifiers.append(quantifier)

            recent_quantifiers.extend(used_quantifiers)
            recent_quantifiers = recent_quantifiers[-self._lexicalization_window_size:]

            self.lexicalize(recent_quantifiers)

            if intention.name not in stats:
                stats[intention.name] = {'#': 0}
            for quantifier in used_quantifiers:
                if quantifier.operator.name not in stats[intention.name]:
                    stats[intention.name][quantifier.operator.name] = 0
                stats[intention.name][quantifier.operator.name] += 1
            stats[intention.name]['#'] += 1

        #for i, s in stats.items():
        #    print(i)
        #    for operator, count in s.items():
        #        if operator != '#':
        #            print(operator, count / s['#'])
        #print()
        #global_stats = {o.name: 0 for o in Grammar.SQUARE}
        #for s in stats.values():
        #    for operator, count in s.items():
        #        if operator != '#':
        #            global_stats[operator] += count / self._sentences_per_speech
        #print(global_stats)

        return sentences

    def generate_true_sentence(self, world):
        predicates = self.grammar.get_lexemes(Predicate)

        intention = Grammar.INTENTIONS[_weighted_choice(INTENTION_WEIGHTS)]

        quantifier = Quantifier('', intention)
        while True:
            qrange, expression = random.sample(list(predicates.values()), 2)
            if quantifier.func(world, (qrange, expression)):
                break

        sentences = self.grammar.how_to_say(intention, qrange, expression)

        weighted_sentences = {sentence: _complexity_weight(sentence) for sentence in sentences}
        sentence = _weighted_choice(weighted_sentences)

        #print(intention.name, '~' + sentence[1].operator.name if sentence[0] is not_ else sentence[0].operator.name)

        return intention, sentence

